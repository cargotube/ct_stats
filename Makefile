REBAR = rebar3

.PHONY: all ct test clean elvis compile


all: compile
	$(REBAR) lint
	$(REBAR) eunit
	$(REBAR) ct
	$(REBAR) cover -v 

clean:
	$(REBAR) cover -r 
	$(REBAR) clean

eunit:
	$(REBAR) eunit
	$(REBAR) cover -v

ct:
	$(REBAR) ct
	$(REBAR) cover -v

tests: elvis eunit ct dialyzer

elvis:
	$(REBAR) lint

dialyzer:
	$(REBAR) dialyzer 

compile:
	$(REBAR) compile


