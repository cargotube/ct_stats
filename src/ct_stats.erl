-module(ct_stats).

-export([
         new/1,
         update/1,
         add_message/3
        ]).

new(Config) ->
    ct_stats_messages_sup:new(Config).

update(Pid) ->
    ct_stats_messages:update(Pid).

add_message(Type, Duration, Pid) ->
    ct_stats_messages:add(Type, Duration, Pid).

