-module(ct_stats_msg_timer).

-behaviour(gen_server).

-export([start_link/0,
         stop/0,

         add_pid/1,
         remove_pid/1,

         init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).


-record(state, {pids = [],
                next_tick = undefined
                }).

-define(DELAY, 100).
-define(TOLERANCE, 1).


add_pid(Pid) ->
    gen_server:call(?MODULE, {add_pid, Pid}).

remove_pid(Pid) ->
    gen_server:call(?MODULE, {remove_pid, Pid}).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, no_params, []).

stop() ->
    gen_server:cast(?MODULE, stop).

init(no_params) ->
    Now = current_ms(),
    {ok, #state{next_tick = Now+  ?DELAY}, ?DELAY}.


handle_call({add_pid, Pid}, _From, #state{pids = Pids} = State)
  when is_pid(Pid) ->
    NewPids = [Pid | lists:delete(Pid, Pids)],
    UpdatedState = State#state{pids = NewPids},
    {Delay, NewState} = calc_timeout_send_tick(UpdatedState),
    {reply, ok, NewState, Delay};
handle_call({remove_pid, Pid}, _From, #state{pids = Pids} = State)
  when is_pid(Pid)  ->
    NewPids = lists:delete(Pid, Pids),
    UpdatedState = State#state{pids = NewPids},
    {Delay, NewState} = calc_timeout_send_tick(UpdatedState),
    {reply, ok, NewState, Delay};
handle_call(_Message, _From, State) ->
    {Delay, NewState} = calc_timeout_send_tick(State),
    {reply, unsupported, NewState, Delay}.


handle_cast(stop, State) ->
    {stop, normal, State};
handle_cast(_Message, State) ->
    {Delay, NewState} = calc_timeout_send_tick(State),
    {noreply, NewState, Delay}.

handle_info(_,  State) ->
    {Delay, NewState} = calc_timeout_send_tick(State),
    {noreply, NewState, Delay}.


calc_timeout_send_tick(#state{next_tick=Next} = State) ->
    Now = current_ms(),
    Delay = Next - Now,
    maybe_tick(Delay, State).

maybe_tick(Delay, #state{next_tick = OldNext} = State)
  when Delay =< ?TOLERANCE ->
    send_tick(State),
    Next = OldNext + ?DELAY,
    calc_timeout_send_tick(State#state{next_tick = Next});
maybe_tick(Delay, State) ->
    {Delay, State}.

send_tick(#state{pids = Pids}) ->
    Fun = fun() ->
                  do_send_tick(Pids)
          end,
    spawn(Fun).

do_send_tick(Pids) ->
    Send = fun(Pid, _) ->
               Pid ! tick
           end,
    lists:foldl(Send, undefined, Pids).

current_ms() ->
   erlang:system_time(millisecond).

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


