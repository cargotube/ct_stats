-module(ct_stats_messages_sup).

-behaviour(supervisor).

-export([new/1]).
-export([start_link/0]).
-export([init/1]).

-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, noparams).

new(Config) ->
    supervisor:start_child(?MODULE, [Config]).

init(noparams) ->
    Procs = [
             cts_messages()
            ],
    Flags = #{ strategy => simple_one_for_one },
    {ok, {Flags, Procs}}.

cts_messages() ->
    #{ id => messagestat,
       start => {ct_stats_messages, start_link, []}
     }.

