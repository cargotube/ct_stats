-module(ct_stats_messages).

-behaviour(gen_server).

-export([
         get_stats/1,

         add/3,
         update/1,

         start_link/1,
         stop/1,
         init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).


-record(state, {
          name = undefined,
          stat_update = 0,
          max_msg_keep = 0,
          stat_keep = 0,

          tick_count = 0,
          entries = [],
          count = 0,
          counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          msg_sec = [],
          avg_msg_sec = 0,

          fastest = 0,
          percentile25 = 0,
          percentile50 = 0,
          percentile75 = 0,
          percentile99 = 0,
          slowest = 0
         }).


get_stats(Pid) ->
    gen_server:call(Pid, get_stats).

add(Type, Duration, Pid) ->
    gen_server:cast(Pid, {add, Type, Duration}).

update(Pid) ->
    gen_server:cast(Pid, update).

stop(Pid) ->
    gen_server:cast(Pid, stop).

start_link(Config) ->
    gen_server:start_link(?MODULE, Config, []).

init(#{name := Name} = Config) when is_binary(Name) ->
    MaxMsgSec = maps:get(max_msg_keep, Config, 3600),
    TimeMs = maps:get(stat_update, Config, 300000),
    MaxKeepSec = maps:get(stat_keep, Config, 86400),
    timer:apply_interval(TimeMs, ?MODULE, update, [self()]),
    ct_stats_msg_timer:add_pid(self()),
    {ok, #state{
                name = Name,
                stat_update = TimeMs,
                max_msg_keep = MaxMsgSec,
                stat_keep = MaxKeepSec,
                msg_sec = create_list(MaxMsgSec, [])
               }}.

handle_call(get_stats, _From, State) ->
    {reply, state_to_map(State), State};
handle_call(_Message, _From, State) ->
    {reply, unsupported, State}.

handle_cast({add, Type, Duration}, #state{entries = Entries,
                                          count = Count} = State) ->
    Now = erlang:system_time(second),
    NewEntries = [ { Duration, Type, Now} | Entries ],
    NewCount = Count + 1,
    {noreply, State#state{entries = NewEntries, count = NewCount} };
handle_cast(update, State) ->
    {noreply, perform_update(State)};
handle_cast(stop, State) ->
    {stop, normal, State};
handle_cast(_Message, State) ->
    {noreply, State}.

handle_info(tick, State) ->
    {noreply, perform_tick(State)};
handle_info({updated, Map}, State) ->
    {noreply, update_with_map(Map, State)};
handle_info(_Message, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ct_stats_msg_timer:remove_pid(self()),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


perform_tick(#state{count = Count, counts = Counts } = State) ->
    NewCounts = [ Count | lists:droplast(Counts)],
    maybe_move_msgs(State#state{ count = 0, counts = NewCounts }).

maybe_move_msgs(#state{ tick_count=9 } = State) ->
    perform_msg_move(State);
maybe_move_msgs(#state{tick_count = Ticks} = State) ->
    State#state{ tick_count=Ticks+1 }.

perform_msg_move(#state{msg_sec = MsgSecs, counts = Counts} = State) ->
    Sum = lists:sum(Counts),
    NewMsgSecs = [ Sum | lists:droplast(MsgSecs) ],
    State#state{msg_sec = NewMsgSecs, tick_count = 0}.


perform_update(#state{entries = []} = State) ->
    State;
perform_update(#state{entries = Entries,
                     stat_keep = MaxKeepStat} = State) ->
    Pid = self(),
    UpdateFun = fun() ->
                        calculate_percentile(Entries, MaxKeepStat, Pid)
                end,
    spawn(UpdateFun),
    State.


calculate_percentile(Entries, StatMaxKeepSec, Pid) ->
    Now = erlang:system_time(second),
    StillOkayTime = Now - StatMaxKeepSec,

    FilterOld =
        fun({_Duration, _Type, Time} = Entry, {List, Oldest}) ->
                case {Time >= StillOkayTime, Time < Oldest} of
                    {true, true}  ->
                        {[ Entry | List], Time};
                    {true, false} ->
                        {[ Entry | List], Oldest};
                    _ ->
                        {List, Oldest}
                end
        end,


    {ActiveEntries, Oldest}  = lists:foldl(FilterOld, {[], Now}, Entries),
    Sort =
        fun({DurationA, _, _}, {DurationB, _, _}) ->
                DurationA =< DurationB
         end,
    Sorted = lists:sort(Sort, ActiveEntries),
    Length = length(Sorted),
    Percentile25 = percentile( 0.25,  Sorted, Length),
    Percentile50 = percentile( 0.5,  Sorted, Length),
    Percentile75 = percentile( 0.75,  Sorted, Length),
    Percentile99 = percentile( 0.99,  Sorted, Length),
    {Fastest, _, _} = lists:nth(1, Sorted),
    {Slowest, _, _} = lists:nth(Length, Sorted),
    AvgMsgSec =  avg_msg_per_sec(Now, Oldest, Length),


    Pid ! {updated, #{
             fastest => Fastest,
             percentile25 => Percentile25,
             percentile50 => Percentile50,
             percentile75 => Percentile75,
             percentile99 => Percentile99,
             slowest => Slowest,
             avg_msg_sec => AvgMsgSec,
             msg_count => Length
            }}.

avg_msg_per_sec(Now, Now, Count) ->
    Count;
avg_msg_per_sec(Now, Oldest, Count) ->
    Count / (Now - Oldest).


update_with_map(#{fastest := undefined, avg_msg_sec := AvgMsgSec}, State) ->
    State#state{ fastest = undefined,
                 percentile25 = undefined,
                 percentile50 = undefined,
                 percentile75 = undefined,
                 percentile99 = undefined,
                 slowest = undefined,
                 avg_msg_sec = AvgMsgSec
               };
update_with_map(#{fastest := Fastest, percentile25 := Percentile25,
                  percentile50 := Percentile50, percentile75 := Percentile75,
                  percentile99 := Percentile99, slowest := Slowest,
                  avg_msg_sec := AvgMsgSec, msg_count := MsgCount},
                #state{name = Name} = State) ->
    lager:info("~s stats: ~.2.0f msg/sec [ ~.2.0f / ~.2.0f / ~.2.0f / ~.2.0f /"
               " * ~.2.0f * / ~.2.0f ] ms; #~p",
               [Name, AvgMsgSec, Fastest, Percentile25, Percentile50,
                Percentile75, Percentile99, Slowest, MsgCount]),
    State#state{ fastest = Fastest,
                 percentile25 = Percentile25,
                 percentile50 = Percentile50,
                 percentile75 = Percentile75,
                 percentile99 = Percentile99,
                 slowest = Slowest,
                 avg_msg_sec = AvgMsgSec
               }.

state_to_map(#state{fastest = Fastest, percentile25 = Perc25,
                    percentile50 = Perc50, percentile75 = Perc75,
                    percentile99 = Perc99, slowest = Slowest }) ->
    #{fastest => Fastest, percentile25 => Perc25, percentile50 => Perc50,
      percentile75 => Perc75, percentile99 => Perc99, slowest => Slowest}.


percentile(_, [],  _) ->
    undefined;
percentile(Percentil, Entries, Length) ->
    Position = percentile_pos(Percentil, Length),
    FPos = simple_floor(Position),
    calc_percentile(FPos, Position - FPos, Entries).


percentile_pos(Percentil, Length) when Percentil > 0, Percentil < 1 ->
    Percentil * Length.

calc_percentile(Pos, Partial, Entries) when is_integer(Pos), Partial < 0.001 ->
    A = get_xn(Pos, Entries),
    B = get_xn(Pos+1, Entries),
    0.5 * (A + B);
calc_percentile(Pos, _Partial, Entries) when is_integer(Pos) ->
   get_xn(Pos + 1, Entries).

get_xn(Pos, Entries) ->
    {Result, _, _} = lists:nth(Pos, Entries),
    Result.

-spec simple_floor(number()) -> integer().
simple_floor(Number) ->
   Integer = round(Number),
   case Integer > Number of
       true -> Integer - 1;
       false -> Integer
   end.

create_list(0, List) ->
    List;
create_list(Count, List) ->
    create_list(Count -1, [ 0 |  List ]).
