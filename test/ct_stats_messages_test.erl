-module(ct_stats_messages_test).
-include_lib("eunit/include/eunit.hrl").

empty_percentile_test() ->
    Config = #{name => <<"test">>},
    {ok, _} = ct_stats_msg_timer:start_link(),
    {ok, Pid} = ct_stats_messages:start_link(Config),
    ok = ct_stats_messages:update(Pid),
    Map = ct_stats_messages:get_stats(Pid),
    ok = ct_stats_messages:stop(Pid),
    timer:sleep(100),
    ok = ct_stats_msg_timer:stop(),
    ok.


single_entry_percentile_test() ->
    Config = #{name => <<"test">>},
    {ok, _} = ct_stats_msg_timer:start_link(),
    {ok, Pid} = ct_stats_messages:start_link(Config),
    ok = ct_stats_messages:add( hello, 0.1, Pid),
    ok = ct_stats_messages:update(Pid),
    timer:sleep(100),
    Map = ct_stats_messages:get_stats(Pid),
    0.1 = maps:get(fastest, Map),
    ok = ct_stats_messages:stop(Pid),
    timer:sleep(100),
    ok = ct_stats_msg_timer:stop(),
    ok.


multiple_entry_percentile_test() ->
    Config = #{name => <<"test">>},
    {ok, _} = ct_stats_msg_timer:start_link(),
    {ok, Pid} = ct_stats_messages:start_link(Config),
    Input = [3.9, 3.3, 4.6, 4.0, 3.8, 3.8, 3.6, 4.6, 4.0, 3.9, 3.9, 3.9, 4.1, 3.7, 3.6, 4.6, 4.0, 4.0, 3.8, 4.1],
    Add = fun(Duration, _) ->
            ct_stats_messages:add(hello, Duration, Pid),
	    ok
          end,
    lists:foldl(Add, ok, Input),
    ok = ct_stats_messages:update(Pid),
    timer:sleep(100),
    Map = ct_stats_messages:get_stats(Pid),
    3.3 = maps:get(fastest, Map),
    4.6 = maps:get(slowest, Map),
    3.8 = maps:get(percentile25, Map),
    3.9 = maps:get(percentile50, Map),
    4.05 = maps:get(percentile75, Map),
    ok = ct_stats_messages:stop(Pid),
    timer:sleep(100),
    ok = ct_stats_msg_timer:stop(),
    ok.


move_msg_test() ->
    Config = #{name => <<"test">>},
    {ok, _} = ct_stats_msg_timer:start_link(),
    {ok, Pid} = ct_stats_messages:start_link(Config),
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    Pid ! tick,
    timer:sleep(100),
    ok = ct_stats_messages:stop(Pid),
    timer:sleep(100),
    ok = ct_stats_msg_timer:stop(),
    ok.
    



garbage_test() ->
    Config = #{name => <<"test">>},
    {ok, _} = ct_stats_msg_timer:start_link(),
    {ok, Pid} = ct_stats_messages:start_link(Config),
    unsupported = gen_server:call(Pid, garbage),
    ok = gen_server:cast(Pid, garbage),
    Pid ! garbage,
    Map = ct_stats_messages:get_stats(Pid),
    true = is_map(Map),
    ok = ct_stats_messages:stop(Pid),
    timer:sleep(100),
    ok = ct_stats_msg_timer:stop(),
    ok.
    
