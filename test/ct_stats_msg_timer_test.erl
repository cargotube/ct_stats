-module(ct_stats_msg_timer_test).
-include_lib("eunit/include/eunit.hrl").


start_stop_test() ->
    {ok, _} = ct_stats_msg_timer:start_link(),
    ok = ct_stats_msg_timer:stop(),
    ok.

garbage_test() ->
    {ok, Pid} = ct_stats_msg_timer:start_link(),
    unsupported = gen_server:call(Pid, garbage),
    ok = gen_server:cast(Pid, garbage),
    Pid ! garbage,
    ok = ct_stats_msg_timer:stop(),
    ok.


tick_test() ->
    {ok, _} = ct_stats_msg_timer:start_link(),
    Fun = fun() -> 
               tick_counter(0)
	  end,
    TickPid = spawn(Fun),
    ok = ct_stats_msg_timer:add_pid(TickPid),
    timer:sleep(1000),
    ok = ct_stats_msg_timer:remove_pid(TickPid),
    TickCount = get_tick_count(TickPid),
    true = is_number(TickCount),
    true = TickCount >= 9,
    true = TickCount < 11,
    timer:sleep(300),
    TickCount2 = get_tick_count(TickPid),
    TickCount2 = TickCount,
    TickPid ! stop,
    ok = ct_stats_msg_timer:stop(),
    ok.

tick_delay_test() ->
    {ok, _} = ct_stats_msg_timer:start_link(),
    Fun = fun() -> 
               tick_delay([])
	  end,
    TickPid = spawn(Fun),
    ok = ct_stats_msg_timer:add_pid(TickPid),
    timer:sleep(1000),
    ok = ct_stats_msg_timer:remove_pid(TickPid),
    ok = ct_stats_msg_timer:stop(),
    TickList = get_tick_list(TickPid),
    TickPid ! stop,
    [First, Second | _] = TickList,
    TimeOne = First - Second,
    {Fastest, Slowest} = calc_min_max_time(TickList, {TimeOne, TimeOne}),
    true = Fastest >= 90,
    true = Slowest =< 110,
    ok.


calc_min_max_time([ One, Two | Tail], Times) ->
    Delay = One - Two,
    calc_min_max_time([ Two | Tail ], update_times(Delay, Times));
calc_min_max_time(_ , Times) ->
    Times.

update_times(Delay, {Fastest, Slowest})
  when Delay > Slowest ->
    {Fastest, Delay};
update_times(Delay, {Fastest, Slowest})
  when Delay < Fastest ->
    {Delay, Slowest};
update_times(Delay, {Fastest, Slowest}) 
  when Delay >= Fastest, Delay =< Slowest ->
    {Fastest, Slowest}.

get_tick_count(Pid) ->
    Pid ! {get_count, self()},
    receive 
        {ticks, Ticks} ->
            Ticks
    end.

get_tick_list(Pid) ->
    Pid ! {get_list, self()},
    receive 
        {list, List} ->
           List 
    end.

tick_counter(stop) ->
    ok;
tick_counter(Ticks) ->
    NewTicks = 
      receive 
        tick ->
          Ticks + 1;
	{get_count, Pid} ->
          Pid ! {ticks, Ticks},
          Ticks;
	stop ->
	  stop
      end,
    tick_counter(NewTicks).


tick_delay(stop) ->
    ok;
tick_delay(List) ->
    NewList = 
      receive
        tick ->
          Now = erlang:system_time(millisecond),
	  [ Now | List ];
	{get_list, Pid} ->
	  Pid ! {list, List},
	  List;
	stop ->
	  stop
      end,
    tick_delay(NewList).
