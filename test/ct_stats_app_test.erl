-module(ct_stats_app_test).
-include_lib("eunit/include/eunit.hrl").

start_stop_test() ->
    application:ensure_all_started(ct_stats),
    ok = application:ensure_started(ct_stats),
    ok = application:stop(ct_stats).


